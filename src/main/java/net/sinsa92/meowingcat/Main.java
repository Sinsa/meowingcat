package net.sinsa92.meowingcat;

import net.sinsa92.meowingcat.utils.ConfigUtils;
import org.apache.log4j.BasicConfigurator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.ExecutionException;

public class Main {
    static Logger logger = LoggerFactory.getLogger(Main.class);

    public static void main(String[] args) throws ExecutionException, InterruptedException {
        Bot bot = new Bot();
        ConfigUtils configUtils = new ConfigUtils();
        BasicConfigurator.configure();
        if(!configUtils.getConfigValue("token").isEmpty()) {
            bot.init(configUtils.getConfigValue("token"));
        } else {
            logger.error("No token provided! Exiting.");
            System.exit(1);
        }
    }
}
