package net.sinsa92.meowingcat.utils;

import net.sinsa92.meowingcat.exceptions.ConfigException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class ConfigUtils {

    public String getConfigValue(String configValue) {
        JSONObject config = new JSONObject(getConfig());
        return config.getString(configValue);
    }

    private String getConfig() {
        StringBuilder stringBuilder = new StringBuilder();
        File configFile = new File("botconfig.json");
        Scanner scanner;
        try {
            scanner = new Scanner(configFile);
        } catch (FileNotFoundException e) {
            throw new ConfigException("Configuration file was not found! Configfile needs to be named \"botconfig.json\" and must be placed in the same directory as the jar.");
        }
        while (scanner.hasNextLine()) {
            stringBuilder.append(scanner.nextLine());
        }
        scanner.close();
        return stringBuilder.toString();
    }
}
