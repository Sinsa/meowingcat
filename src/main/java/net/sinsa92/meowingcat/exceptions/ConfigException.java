package net.sinsa92.meowingcat.exceptions;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ConfigException extends RuntimeException{
    Logger logger = LoggerFactory.getLogger(ConfigException.class);

    public ConfigException(String reason) {
        super(reason);
        logger.error("Configuration Error thrown! Reason: {}",reason);
    }
}
