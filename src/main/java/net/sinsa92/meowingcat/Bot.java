package net.sinsa92.meowingcat;

import net.sinsa92.meowingcat.utils.ConfigUtils;
import org.javacord.api.DiscordApi;
import org.javacord.api.DiscordApiBuilder;
import org.javacord.api.entity.channel.ServerTextChannel;
import org.javacord.api.entity.channel.ServerTextChannelUpdater;
import org.javacord.api.entity.emoji.Emoji;
import org.javacord.api.entity.intent.Intent;
import org.javacord.api.entity.message.Message;
import org.javacord.api.entity.permission.Permissions;
import org.javacord.api.entity.user.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.ExecutionException;

public class Bot {
    Logger logger = LoggerFactory.getLogger(Bot.class);
    ConfigUtils configUtils = new ConfigUtils();

    public void init(String token) throws InterruptedException, ExecutionException {
        DiscordApi api = new DiscordApiBuilder().setToken(token).addIntents(Intent.MESSAGE_CONTENT, Intent.GUILD_MEMBERS).login().join();
        logger.info("<init> I am ready");

        api.addServerVoiceChannelMemberJoinListener(event -> {
            if (String.valueOf(event.getChannel().getId()).equals(configUtils.getConfigValue("observingVoiceChannel"))) {
                ServerTextChannel channel = api.getChannelById(configUtils.getConfigValue("accordingTextChannel")).get().asServerTextChannel().get();
                ServerTextChannelUpdater updater = channel.createUpdater();
                updater.addPermissionOverwrite(event.getUser(), Permissions.fromBitmask(3072));
                updater.update();
            }
        });

        api.addServerVoiceChannelMemberLeaveListener(event -> {
            if (String.valueOf(event.getChannel().getId()).equals(configUtils.getConfigValue("observingVoiceChannel"))) {
                ServerTextChannel channel = api.getChannelById(configUtils.getConfigValue("accordingTextChannel")).get().asServerTextChannel().get();
                ServerTextChannelUpdater updater = channel.createUpdater();
                updater.removePermissionOverwrite(event.getUser());
                updater.update();
            }
        });

        long JONI_ID=315132690460311553L;
        api.addReactionAddListener(event -> {
            if (event.getUserId() == JONI_ID && event.getEmoji().getMentionTag().equals("<:extrem:943653048364978218>")) {
                logger.debug("milch");
                ServerTextChannel reactionChannel = event.getServerTextChannel().get();
                try {
                    Message reactionMessage = api.getMessageById(event.getMessageId(), reactionChannel).get();
                    User joni = api.getUserById(JONI_ID).get();
                    Emoji extremEmoji = api.getCustomEmojiById("943653048364978218").get();
                    reactionMessage.removeReactionsByEmoji(joni, extremEmoji);
                } catch (InterruptedException | ExecutionException e) {}
            }
        });
    }
}
